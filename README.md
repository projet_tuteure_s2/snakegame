# Projet tuteuré semestre 2

Title  : **Snakevenom**

![](src/main/resources/img/SnakeVenomLogo.png)

## Introduction

Dépôt Gitlab contenant les sources d'un jeu Snake en langage Java, utilisant la biliothèque JavaFX.
La version 8 de Java est utilisé.

## Phases de développement

1. Production d'un jeu basique, avec des fonctionnalités basiques
2. Production d'un jeu plus avancé, avec cetaines fonctionnalités multijoueur, de classements, etc ...
3. Ajout cosmétique et d'easter eggs

## \#KnowYourMemes

- ![](src/main/resources/img/elements/obstacle_hildenpixel.png)
- ![](src/main/resources/img/elements/food_egg.png)
- ![](src/main/resources/img/elements/obstacle_elpaito.png)
- ![](src/main/resources/img/elements/obstacle_trouhehehe.png)
