package fr.univ_fcomte.snakegame;

import javafx.application.Application;
import javafx.stage.Stage;
import fr.univ_fcomte.snakegame.ui.*;

public class App extends Application {
	
	
	/**
	 * Define a static var to get the main stage ANYWHERE.
	 */
	private static Stage stage;
	/**
	 * Define the static final windows size.
	 */
	private static final int[] DIMENSIONS = { 960, 580 };
	

	/**
	 * Main method called by java to start the program.
	 */
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		
		App.stage = stage;
		
		Game game = new Game(stage, false);
		Menu menu = new Menu(game, stage);

		game.setMenu(menu);
		new ControlButtonCloseMenu(game, menu);
		new ControlButtonLoginMenu(game, menu);
		new ControlButtonOptionMenu(game, menu);
		new ControlButtonStartMenu(game, menu);
		new ControlButtonToolBar(game, menu);
		menu.display();
	}
	
	/**
	 * @return the actual stage.
	 */
	public static Stage getStage() {
		return stage;
	}

	/**
	 * @return the windows size.
	 */
	public static int[] getDimensions() {
		return App.DIMENSIONS;
	}
	
}

