package fr.univ_fcomte.snakegame.enums;

import fr.univ_fcomte.snakegame.elements.snake.parts.BodyPart;

public enum EnumCorner {

	SE("South-East", EnumFacing.S, EnumFacing.E, 90),
	ES("East-South", EnumFacing.E, EnumFacing.S, 270),
	WS("West-South", EnumFacing.W, EnumFacing.S, 180),
	SW("South-West", EnumFacing.S, EnumFacing.W, 0),
	NW("North-West", EnumFacing.N, EnumFacing.W, 270),
	WN("West-North", EnumFacing.W, EnumFacing.N, 90),
	EN("East-North", EnumFacing.E, EnumFacing.N, 0),
	NE("North-East", EnumFacing.N, EnumFacing.E, 180);
	
	private String name;
	private EnumFacing prev;
	private EnumFacing next;
	private int rotation;
	
	EnumCorner(String name, EnumFacing prev, EnumFacing next, int rotation) {
		this.name = name;
		this.prev = prev;
		this.next = next;
		this.rotation = rotation;
	}
	
	EnumCorner(String name, BodyPart prev, BodyPart next, int rotation) {
		this(name, prev.getDirection(), next.getDirection(), rotation);
	}
	
	public int getRotation() {
		return this.rotation;
	}
	
	public EnumFacing getPrev() {
		return prev;
	}
	
	public void setPrev(EnumFacing prev) {
		this.prev = prev;
	}
	
	public EnumFacing getNext() {
		return next;
	}
	
	public void setNext(EnumFacing next) {
		this.next = next;
	}
	
	public String toString() {
		return this.name;
	}
	
	public static EnumCorner createCorner(EnumFacing head, EnumFacing corner) {
		switch(head) {
			case N:
				if(corner == EnumFacing.E) {
					return NE;
				}
				return NW;
			case E:
				if(corner == EnumFacing.N) {
					return EN;
				}
				return ES;
			case S:
				if(corner == EnumFacing.E) {
					return SE;
				}
				return SW;
			case W:
				if(corner == EnumFacing.N) {
					return WN;
				}
				return WS;
			default:
				System.out.println("DEFAULT CASE!");
				return SE;
		}
	}
}