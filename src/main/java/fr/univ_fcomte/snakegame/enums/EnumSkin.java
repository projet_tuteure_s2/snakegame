package fr.univ_fcomte.snakegame.enums;

public enum EnumSkin {
	venom,
	spider,
	classic;
	
	public static EnumSkin getRandomSkin() {
		return values()[(int)(Math.random() * values().length)];
	}
}