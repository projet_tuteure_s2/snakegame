package fr.univ_fcomte.snakegame.enums;

public enum EnumMusic {
	
	Music01("audio/music_01.mp3");
	
	private String path;
	
	EnumMusic(String path) {
		this.path = path;
	}
	
	public static String getRandomMusic() {
		return values()[(int)(Math.random() * values().length)].toString();
	}
	
	@Override
	public String toString() {
		return this.path;
	}
	
}
