package fr.univ_fcomte.snakegame.enums;

public enum EnumObstacle {
	Rock("/img/elements/obstacle_rock.png"),
	Rock1("/img/elements/obstacle_rock1.png"),
	Bush("/img/elements/obstacle_bush.png"),
	RoundTimbers("/img/elements/obstacle_roundtimbers.png"),
	HildenPixel("/img/elements/obstacle_hildenpixel.png"),
	TrouHehehe("/img/elements/obstacle_trouhehehe.png"),
	ElPaito("/img/elements/obstacle_elpaito.png");
	
	private String path;
	
	private EnumObstacle(String path) {
		this.path = path;
	}

	public String getPath() {
		return path;
	}
}
