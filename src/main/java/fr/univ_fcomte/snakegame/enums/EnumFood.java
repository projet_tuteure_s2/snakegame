package fr.univ_fcomte.snakegame.enums;

public enum EnumFood {
	
	Apple("Apple",
		10,
		.98,
		"/img/elements/food_apple.png"),
	Egg("Egg",
		100,
		1.2,
		"/img/elements/food_egg.png"),
	Mouse("Mouse",
		300,
		1.1,
		"/img/elements/food_mouse.png"),
	Orange("Orange",
		50,
		.97,
		"/img/elements/food_orange.png"),
	Pineapple("Pineapple",
		200,
		.95,
		"/img/elements/food_pineapple.png"),
	Psylotrope("Psylotrope",
		-100,
		.8,
		"/img/elements/food_psylotrope.png"),
	HildenPixel("Hildenpixel",
		50,
		.9,
		"/img/elements/food_hildenpixel.png"),
	SnakeVenom("Snakevenom",
		1000,
		.7,
		"/img/elements/food_snakevenom.png");
	
	private String name;
	private int scoreMod;
	private double speedModifier;
	private final String imagePath;
	
	EnumFood(String name, int scoreMod, double speedModifier, String imagePath) {
		this.name = name;
		this.scoreMod = scoreMod;
		this.speedModifier = speedModifier;
		this.imagePath = imagePath;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getScoreMod() {
		return this.scoreMod;
	}
	
	public String getImagePath() {
		return this.imagePath;
	}
	
	public double getSpeedModifier() {
		return this.speedModifier;
	}
	
	public static EnumFood getRandomFood() {
		return values()[(int)(Math.random() * values().length)];
	}
	
}
