package fr.univ_fcomte.snakegame.enums;

import fr.univ_fcomte.snakegame.elements.snake.parts.BodyPart;

public enum EnumFacing {
	S("South", 0),
	W("West", 90),
	N("North", 180),
	E("East", 270);
	
	private String name;
	private int degree;
	
	EnumFacing(String name, int degree) {
		this.name = name;
		this.degree = degree;
	}
	
	public int getRotation() {
		return this.degree;
	}
		
	public static EnumFacing getTailDirection(BodyPart previous, int y, int x) {
		int px = previous.getLocationX();
		int py = previous.getLocationY();
		
		if(px==x) 
			if(py>y) 
				return S;
			else 
				return N;
		
		if(py==y)
			if(px>x) 
				return E;
			else 
				return W;
		
		return previous.getDirection();
	}
		
	public boolean isOppositeDirection(EnumFacing direction) {
		if(this.equals(EnumFacing.N) && direction.equals(EnumFacing.S)) {
			return true;
		}
		if(this.equals(EnumFacing.S) && direction.equals(EnumFacing.N)) {
			return true;
		}
		if(this.equals(EnumFacing.E) && direction.equals(EnumFacing.W)) {
			return true;
		}
		if(this.equals(EnumFacing.W) && direction.equals(EnumFacing.E)) {
			return true;
		}
		return false;
	}
	
	public EnumFacing getOppositeDirection() {
		if(this.isOppositeDirection(EnumFacing.S))
			return EnumFacing.S;
		if(this.isOppositeDirection(EnumFacing.N))
			return EnumFacing.N;
		if(this.isOppositeDirection(EnumFacing.W))
			return EnumFacing.W;
		return EnumFacing.E;
	}
	
	@Override
	public String toString() {
		return this.name;
	}
}
