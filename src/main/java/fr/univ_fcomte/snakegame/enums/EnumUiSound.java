package fr.univ_fcomte.snakegame.enums;

public enum EnumUiSound {
	
	Music01("audio/ui_01.mp3"),
	Music02("audio/ui_02.mp3");
	
	private String path;
	
	EnumUiSound(String path) {
		this.path = path;
	}
	
	public static String getRandomSound() {
		return values()[(int)(Math.random() * values().length)].toString();
	}
	
	@Override
	public String toString() {
		return this.path;
	}
	
}
