package fr.univ_fcomte.snakegame.enums;

public enum EnumFloor {
	brick,
	dirt,
	grass1,
	grass2,
	grass3,
	water,
	wood;

	public static EnumFloor getRandomSkin() {
		return values()[(int)(Math.random() * values().length)];
	}
}