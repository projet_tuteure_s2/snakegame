package fr.univ_fcomte.snakegame.elements;

import java.util.Random;

import fr.univ_fcomte.snakegame.enums.EnumObstacle;

public class Obstacle extends Element {
	
	public Obstacle() {
		super(EnumObstacle.values()[new Random().nextInt(EnumObstacle.values().length)].getPath());
	}
}
