package fr.univ_fcomte.snakegame.elements;

import fr.univ_fcomte.snakegame.enums.EnumFood;

public class Food extends Element {
	
	private String name;
	private int scoreMod;
	private double speedModifier;
	
	/**
	 * Default constructor with apple
	 */
	public Food() {
		this(EnumFood.getRandomFood());
	}
	
	/**
	 * Construct the food according to properties from the enum
	 * @param f
	 */
	public Food(EnumFood f) {
		super(f.getImagePath());
		this.name = f.getName();
		this.scoreMod = f.getScoreMod();
		this.speedModifier = f.getSpeedModifier();
	}
	
	public Food(Food food) {
		super(food);
		this.scoreMod = food.scoreMod;
		this.speedModifier = food.speedModifier;
	}
	
	public int getScore() {
		return this.scoreMod;
	}
	
	public double getSpeedModifier() {
		return this.speedModifier;
	}

	public String getName() {
		return this.name;
	}
	
}
