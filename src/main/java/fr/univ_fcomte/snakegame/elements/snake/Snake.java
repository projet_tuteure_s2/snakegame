package fr.univ_fcomte.snakegame.elements.snake;

import java.util.ArrayList;
import java.util.List;

import fr.univ_fcomte.snakegame.Game;
import fr.univ_fcomte.snakegame.elements.Element;
import fr.univ_fcomte.snakegame.elements.Floor;
import fr.univ_fcomte.snakegame.elements.Food;
import fr.univ_fcomte.snakegame.elements.Obstacle;
import fr.univ_fcomte.snakegame.elements.snake.parts.BodyCorner;
import fr.univ_fcomte.snakegame.elements.snake.parts.BodyPart;
import fr.univ_fcomte.snakegame.elements.snake.parts.Head;
import fr.univ_fcomte.snakegame.elements.snake.parts.Tail;
import fr.univ_fcomte.snakegame.enums.EnumCorner;
import fr.univ_fcomte.snakegame.enums.EnumFacing;
import fr.univ_fcomte.snakegame.enums.EnumSkin;
import fr.univ_fcomte.snakegame.map.Map;
import fr.univ_fcomte.snakegame.ui.GameUi;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class Snake extends Element {
	
	private String name;
	private int length;
	private int snakeId;
	private List<BodyPart> bodyParts;
	private int eatScore;

	private Map map;
	private Game game;
	private EnumFacing direction;
	private BodyPart ll;
	
	private boolean hasTurned;
	
	private EnumSkin skin;
	
	private MediaPlayer mediaPlayer;
	private Media sfxEat;
	private Media bahlesoeufs;
	private Media sfxCrash;
	
	private final static int DEFAULT_LENGTH = 3;
	
	/**
	 * Default constructor for snake with initial size (3 body parts)
	 */
	public Snake(Game game, Map map, GameUi gameUi, int snakeId) {
		this(game, map, gameUi, DEFAULT_LENGTH, snakeId, EnumSkin.getRandomSkin());
	}
	
	/**
	 * Default constructor for snake with initial size (3 body parts)
	 */
	public Snake(Game game, Map map, GameUi gameUi, int snakeId, EnumSkin skin) {
		this(game, map, gameUi, DEFAULT_LENGTH, snakeId, skin);
	}
	
	public Snake(Game game, Map map, GameUi gameUi, int baseLength, int snakeId, EnumSkin skin) {
		// SnakeId for multiplayer
		this.snakeId = snakeId;
		
		// Game objects
		this.map = map;
		this.game = game;
		
		// Snake properties
		this.bodyParts = new ArrayList<BodyPart>();
		this.direction = EnumFacing.W;
		this.hasTurned = false;
		
		game.setCurrentSnake(this);
		
		this.setSkin(skin);
		
		// Snake body init
		this.initBody(baseLength);
		
		this.initSfx();
		
	}
	
	
	/**
	 * Init body factored outside of default snake constructor
	 * Could we need it for game reset ?
	 */
	public void initBody(int baseLength) {
		this.bodyParts.add(new Head(getSkin()));
		for(int i = 0; i < baseLength - 2; i++) {
			ll = new BodyPart(getSkin());
			this.bodyParts.add(ll);
		}
		this.bodyParts.add(new Tail(getSkin()));
		
		for(int i = 0; i<bodyParts.size(); i++) {
			bodyParts.get(i).setPartId(i);
		}
		
		for(BodyPart bp : bodyParts) {
			bp.setDirection(this.direction);
		}
	}
	
	private void initSfx() {
		this.sfxEat = new Media(getClass().getClassLoader().getResource("audio/sfx_eat.mp3").toExternalForm());
		this.sfxCrash = new Media(getClass().getClassLoader().getResource("audio/sfx_crash.mp3").toExternalForm());
		this.bahlesoeufs = new Media(getClass().getClassLoader().getResource("audio/bahlesoeufs.mp3").toExternalForm());
	}
	
	private EnumSkin easterEgg() {
		return EnumSkin.getRandomSkin();
	}

	// ===========================================================================================================//
	// MOVEMENTS
	// ===========================================================================================================//
	
	/**
	 * Move the snake according to the facing of the head
	 * (S, W, N or E)
	 */
	public void move() {
		Head ph = this.getHead();
		// Here we only moving the head and tail, right ?
		int previousHeadX = ph.getLocationX();
		int previousHeadY = ph.getLocationY();
		int nextHeadX = previousHeadX;
		int nextHeadY = previousHeadY;

		// Let's compute the new head position first
		switch(this.direction) {
			case S:
				nextHeadY++;
				break;
			case W:
				nextHeadX--;
				break;
			case N:
				nextHeadY--;
				break;
			case E:
				nextHeadX++;
				break;
		}
		// Now, are we in a game over state ?
		if(this.isOnWall(nextHeadY, nextHeadX) || this.isOnItself(nextHeadY, nextHeadX) || this.isOnObstacle(nextHeadY, nextHeadX))  {
			// TODO Trigger gameover below
			this.game.gameOver();
			if(!this.game.getMenu().isMuted()) {
				this.mediaPlayer = new MediaPlayer(this.sfxCrash);
				this.mediaPlayer.setVolume(0.04);
				this.mediaPlayer.play();
			}
			
		}
		// Or, are we eating foor ?
		else if(this.isOnFood(nextHeadY, nextHeadX)) {
			//Eat score
			try {
				//this.upEatScore();
				Game.getGame().getGameUi().upEatScore();
				Game.getGame().getGameUi().upScore(this.map.getCurrentFood().getScore());
			} catch(Exception e) {
				System.out.println(e);
			}
			// SFX ---
			if(!this.game.getMenu().isMuted()) {
				this.mediaPlayer = new MediaPlayer(this.map.getCurrentFood().getName().toLowerCase().contains("hildenpixel") ? this.bahlesoeufs : this.sfxEat);
				this.mediaPlayer.play();
			}			
			// Tell the map to spawn new food
			this.map.spawnScatter(new Food());
			// Then increase the speed
			this.game.increaseSpeed(this.getFacingFood(nextHeadY, nextHeadX).getSpeedModifier());
			// Move head
			this.map.spawnElement(this.getHead(), nextHeadY, nextHeadX);
			// Add body to list of bodyParts and spawn it
			this.grow(previousHeadY, previousHeadX);

			
		}
		// Instead, we trigger the normal behavior
		else {
			int previousTailY = this.getTail().getLocationY();
			int previousTailX = this.getTail().getLocationX();

			// Place floor at previous tail location
			this.map.spawnElement(new Floor(), previousTailY, previousTailX);

			// Then we compute the previous tail location and it's next one
			// (previous for spawning floor)
			int nextTailY = this.bodyParts.get(this.bodyParts.size() - 2).getLocationY();
			int nextTailX = this.bodyParts.get(this.bodyParts.size() - 2).getLocationX();

			// First we move the head
			this.map.spawnElement(this.getHead(), nextHeadY, nextHeadX);
			
			// Move body and set rotation
			EnumFacing previousBodyRotation = this.getHead().getDirection();

			this.bodyParts.remove(this.bodyParts.size() - 2);
			if(hasTurned) {
				BodyPart old = this.getBodyParts().get(1);
				if(old.isCorner()) {
					this.bodyParts.add(1, new BodyCorner(getSkin()));
					this.bodyParts.get(1).setDirection(EnumCorner.createCorner(this.getHead().getDirection(), old.getDirection()));
					((BodyCorner)this.bodyParts.get(1)).setCorner(EnumCorner.createCorner(this.getHead().getDirection(), old.getDirection()));
					this.map.spawnElement(this.bodyParts.get(1), previousHeadY, previousHeadX);
				}else {
					this.bodyParts.add(1, new BodyCorner(getSkin()));
					this.bodyParts.get(1).setDirection(EnumCorner.createCorner(this.getHead().getDirection(), old.getDirection()));
					((BodyCorner)this.bodyParts.get(1)).setCorner(EnumCorner.createCorner(this.getHead().getDirection(), old.getDirection()));
					this.map.spawnElement(this.bodyParts.get(1), previousHeadY, previousHeadX);
					this.hasTurned=false;
				}
			}
			else {
				this.bodyParts.add(1, new BodyPart(getSkin()));
				this.bodyParts.get(1).setDirection(previousBodyRotation);
				this.map.spawnElement(this.bodyParts.get(1), previousHeadY, previousHeadX);
			}
			
			// Move tail to next location
			this.getTail().setDirection(EnumFacing.getTailDirection(bodyParts.get(bodyParts.size()-2), nextTailY, nextTailX));

			this.map.spawnElement(this.getTail(), nextTailY, nextTailX);
			
		}
		ll = this.getBodyParts().get(1);
		this.hasTurned = false;
	}
	
	/**
	 * Turn the snake according to the player's input from keyboard
	 * -- Replace Exception with ReachObstacleException
	 */
	public void turn(EnumFacing direction) {
		if(this.hasTurned || this.direction.isOppositeDirection(direction) || this.direction.equals(direction)) {
			return;
		}
		this.direction = direction;
		this.getHead().setDirection(this.direction);
		this.hasTurned = true;
	}
	
	/**
	 * Increment the body size
	 */
	public void grow(int y, int x) {
		EnumFacing h = this.bodyParts.get(0).getDirection();
		
		this.bodyParts.add(1, new BodyPart(getSkin()));
		this.bodyParts.get(1).setDirection(h);
		this.map.spawnElement(this.bodyParts.get(1), y, x);

		this.bodyParts.get(1).setPartId(1);
		updateIds();
		if(this.map.isWin()) {
			this.game.gameWin();
			if(!this.game.getMenu().isMuted()) {
				this.mediaPlayer = new MediaPlayer(this.sfxCrash);
				this.mediaPlayer.play();
			}
		}
	}
	
	public void updateIds() {
		for(int i = 2; i<this.getBodyParts().size()-1; i++) {
			this.getBodyParts().get(i).setPartId(i);
		}
	}

	public void upEatScore() {
		this.eatScore += 1;
		Game.getGame().getGameUi().upEatScore();
	}
	// ===========================================================================================================//
	// SNAKE STATE
	// ===========================================================================================================//
	
	/**
	 * Is the snake on an non existing cell ?
	 * If so, gameover !
	 *
	 * @param y
	 * @param x
	 * @return
	 */
	public boolean isOnWall(int y, int x) {
		return x < 0 || x >= this.map.getSize() || y < 0 || y >= this.map.getSize();
	}
	
	/**
	 * Is the snake on a cell containing an obstacle ?
	 * If so, gameover !
	 *
	 * @param y
	 * @param x
	 * @return
	 */
	public boolean isOnObstacle(int y, int x) {
		return this.map.getChildren().get(this.map.coordinatesToIndex(y, x)) instanceof Obstacle;
	}
	
	/**
	 * Is the snake on a cell containing one of its body part ?
	 * If so, gameover !
	 *
	 * @param y
	 * @param x
	 * @return
	 */
	public boolean isOnItself(int y, int x) {
		return this.map.getChildren().get(this.map.coordinatesToIndex(y, x)) instanceof BodyPart && !(this.map.getChildren().get(this.map.coordinatesToIndex(y, x)) instanceof Tail);
	}
	
	/**
	 * Is the snake on a cell containing a food object ?
	 *
	 * @param y
	 * @param x
	 * @return
	 */
	public boolean isOnFood(int y, int x) {
		return this.map.getChildren().get(this.map.coordinatesToIndex(y, x)) instanceof Food;
	}
	
	// ===========================================================================================================//
	// GETTERS
	// ===========================================================================================================//
	
	/**
	 * Return the id for the current snake
	 *
	 * @return
	 */
	public int getSnakeId() {
		return this.snakeId;
	}
	
	/**
	 * Return the snake's name
	 *
	 * @return
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Get length of the main body
	 *
	 * @return
	 */
	public int getBodyLength() {
		return this.length - 2; // - tail - head
	}
	
	/**
	 * Get length of the snake :; length of the body part array
	 *
	 * @return
	 */
	public int getLength() {
		return this.length;
	}
	
	/**
	 * Get the snake body
	 */
	public List<BodyPart> getBodyParts() {
		return this.bodyParts;
	}
	
	/**
	 * Get first body part :: head
	 *
	 * @return
	 */
	public Head getHead() {
		return (Head) this.bodyParts.get(0);
	}
	
	/**
	 * Get last body part :: tail
	 *
	 * @return
	 */
	public Tail getTail() {
		return (Tail) this.bodyParts.get(this.bodyParts.size() - 1);
	}
	
	public BodyPart getLl() {
		return ll;
	}	
	
	public BodyPart getBodyPartFromId(int id) {
		if(id>this.length) 
			return null;
		for(BodyPart bp : this.getBodyParts())
			if(bp.getPartId()==id)
				return bp;
		return null;
	}
	
	public Food getFacingFood(int y, int x) {
		return (Food) this.map.getElements()[y][x];
	}

	public int getEatScore() {
		return eatScore;
	}
	
	public EnumSkin getSkin() {
		return this.game.getCurrentSnake().getName()==null ? skin : this.game.getCurrentSnake().getName().equals("polymorph") ? easterEgg() : skin;
	}

	// ===========================================================================================================//
	// SETTERS
	// ===========================================================================================================//

	public void setSkin(EnumSkin skin) {
		this.skin = skin;
	}

	public void setLl(BodyPart ll) {
		this.ll = ll;
	}

	/**
	 * Apply player name to the current snake
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	public void setEatScore(int eatScore) {
		this.eatScore = eatScore;
	}
}
