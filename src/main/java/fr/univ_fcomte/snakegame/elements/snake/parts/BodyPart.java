package fr.univ_fcomte.snakegame.elements.snake.parts;

import fr.univ_fcomte.snakegame.elements.Element;
import fr.univ_fcomte.snakegame.enums.EnumSkin;

public class BodyPart extends Element {
	
	private int id;
	
	public BodyPart(EnumSkin skin) {
		super("/img/elements/skins/snake_bodyPart_"+skin.name()+".gif");
	}
	
	public BodyPart(String path) {
		super(path);
	}
	
	public BodyPart(BodyPart bodyPart) {
		super(bodyPart);
	}
	
	public boolean isCorner() {
		return this instanceof BodyCorner;
	}

	public boolean isHead() {
		return this instanceof Head;
	}

	public boolean isTail() {
		return this instanceof Tail;
	}
	
	public int getPartId() {
		return this.id;
	}
	
	public void setPartId(int id) {
		this.id=id;
	}
	
	
	public static String getRandomBodyPart() {
		return "/img/elements/skins/snake_bodyPart_"+EnumSkin.getRandomSkin().name()+".png";
	}
}
