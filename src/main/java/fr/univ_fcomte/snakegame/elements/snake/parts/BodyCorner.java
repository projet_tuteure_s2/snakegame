package fr.univ_fcomte.snakegame.elements.snake.parts;

import fr.univ_fcomte.snakegame.enums.EnumCorner;
import fr.univ_fcomte.snakegame.enums.EnumSkin;

public class BodyCorner extends BodyPart {
	
	EnumCorner corner;
	
	public BodyCorner(EnumSkin skin) {
		super("/img/elements/skins/snake_bodyCorner_"+skin.name()+".png");
	}
	
	public BodyCorner(BodyCorner bodyPart) {
		super(bodyPart);
	}
	
	public BodyCorner(BodyCorner bodyCorner, int rotation) {
		this(bodyCorner);
		this.setRotate(rotation);
	}

	public EnumCorner getCorner() {
		return corner;
	}

	public void setCorner(EnumCorner corner) {
		this.corner = corner;
	}
	
	public static String getRandomCorner() {
		return "/img/elements/skins/snake_bodyCorner_"+EnumSkin.getRandomSkin().name()+".png";
	}
	
}
