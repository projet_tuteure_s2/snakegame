package fr.univ_fcomte.snakegame.elements.snake.parts;

import fr.univ_fcomte.snakegame.enums.EnumSkin;

public class Tail extends BodyPart {
		
	public Tail(EnumSkin skin) {
		super("/img/elements/skins/snake_tail_"+skin.name()+".png");
	}
	
	public Tail(Tail tail) {
		super(tail);
	}
	
	public static String getRandomTail() {
		return "/img/elements/skins/snake_tail_"+EnumSkin.getRandomSkin().name()+".png";
	}
}
