package fr.univ_fcomte.snakegame.elements.snake.parts;

import fr.univ_fcomte.snakegame.enums.EnumSkin;

public class Head extends BodyPart {
	
	public Head(EnumSkin skin) {
		super("/img/elements/skins/snake_head_"+skin.name()+".png");
	}
	
	public Head(Head head) {
		super(head);
	}
	
	public static String getRandomHead() {
		return "/img/elements/skins/snake_head_"+EnumSkin.getRandomSkin().name()+".png";
	}
}
