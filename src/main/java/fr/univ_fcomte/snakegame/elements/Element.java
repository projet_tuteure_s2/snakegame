package fr.univ_fcomte.snakegame.elements;

import fr.univ_fcomte.snakegame.App;
import fr.univ_fcomte.snakegame.Game;
import fr.univ_fcomte.snakegame.elements.snake.parts.BodyCorner;
import fr.univ_fcomte.snakegame.elements.snake.parts.Head;
import fr.univ_fcomte.snakegame.elements.snake.parts.Tail;
import fr.univ_fcomte.snakegame.enums.EnumCorner;
import fr.univ_fcomte.snakegame.enums.EnumFacing;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Element extends ImageView {
	
	private final int IMAGE_SIZE = (int)(App.getDimensions()[1] / 10);
	protected EnumFacing direction;
	
	private int locationX;
	private int locationY;
	
	/**
	 * Let's have a default tile icon
	 */
	public Element() {
		this("/img/elements/default.png");
	}
	
	/**
	 * We need a custom constructor for different elements
	 * @param path
	 */
	public Element(String path) {
		this.setFitHeight(this.IMAGE_SIZE);
		this.setFitWidth(this.IMAGE_SIZE);
		try {
			this.setImage(new Image(path));
			this.direction = EnumFacing.S;
			this.setRotate(this.direction.getRotation());
		} catch(Exception e) {
			System.out.println(e);
		}
	}
	
	public Element(Element element) {
		this.setImage(
			Game.getGame().getCurrentSnake().getName().equals("polymorph") ?
					element instanceof Tail ? new Image(Tail.getRandomTail()) 
							: element instanceof Head ? new Image(Head.getRandomHead()) 
									: element instanceof BodyCorner ? new Image(BodyCorner.getRandomCorner()) 
												: element.getImage()
														: element.getImage());
		this.setFitHeight(element.getFitHeight());
		this.setFitWidth(element.getFitWidth());
		this.locationY = element.getLocationY();
		this.locationX = element.getLocationX();
		this.setDirection(element.getDirection());
	}
	
	public Element(Element element, int rotation) {
		this(element);
		this.setRotate(rotation);
	}
	
	// ========================================================================================================== //
	// GETTERS
	// ========================================================================================================== //

	/**
	 * Return the current x location
	 * @return
	 */
	public int getLocationX() {
		return this.locationX;
	}
	
	/**
	 * Return the current y location
	 * @return
	 */
	public int getLocationY() {
		return this.locationY;
	}
	
	/**
	 * Return that direction !
	 * @return
	 */
	public EnumFacing getDirection() {
		return this.direction;
	}
	
	// ========================================================================================================== //
	// SETTERS
	// ========================================================================================================== //
	
	/**
	 * Update the X location
	 * @param locationX
	 */
	public void setLocationX(int locationX) {
		this.locationX = locationX;
	}
	
	/**
	 * Update the Y location
	 */
	public void setLocationY(int locationY) {
		this.locationY = locationY;
	}
	
	/**
	 * Set the direction
	 */
	public void setDirection(EnumFacing direction) {
		this.setRotate((this.direction = direction).getRotation());
	}
	
	
	/**
	 * Set the corner direction
	 */
	public void setDirection(EnumCorner direction) {
		this.direction = direction.getNext();
		this.setRotate(direction.getRotation());
	}
	
}
