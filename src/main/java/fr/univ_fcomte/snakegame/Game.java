package fr.univ_fcomte.snakegame;

import fr.univ_fcomte.snakegame.elements.Floor;
import fr.univ_fcomte.snakegame.elements.Food;
import fr.univ_fcomte.snakegame.elements.Obstacle;
import fr.univ_fcomte.snakegame.enums.EnumFloor;
import fr.univ_fcomte.snakegame.enums.EnumMusic;
import fr.univ_fcomte.snakegame.ui.GameUi;
import fr.univ_fcomte.snakegame.ui.Menu;
import fr.univ_fcomte.snakegame.map.Map;
import fr.univ_fcomte.snakegame.elements.snake.Snake;
import javafx.animation.AnimationTimer;
import javafx.scene.input.KeyEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Game {

	protected Map map;
	protected GameUi gameUi;

	protected Snake snakeOne;
	protected Snake snakeTwo;
	protected SnakeController snakeControllerOne;
	protected SnakeController snakeControllerTwo;
	protected Snake currentSnake;
	protected SnakeController currentSnakeController;

	private boolean isMultiplayer;

	private MediaPlayer musicManager;
	private AnimationTimer animation;
	private Media music;

	private int score;

	private static Game game;
	private static Menu menu;

	// Speed base correspond to the amout of millisecond between each event tick
	private final double SPEED_BASE = 1000;
	protected double speedModifier = 1;
	protected double speed;
	public static String username;

	/**
	 * Game initialization
	 */
	public Game(Stage stage, boolean isMultiplayer) {

		this.isMultiplayer = isMultiplayer;

		this.map = new Map(10, EnumFloor.getRandomSkin());
		this.gameUi = new GameUi(stage, this.map, this);

		// Let's init our snake for singleplayer
		this.snakeOne = new Snake(this, this.map, this.gameUi, 0);
		this.snakeControllerOne = new SnakeController(this.snakeOne);
		this.gameUi.getScene().addEventHandler(KeyEvent.KEY_PRESSED, this.snakeControllerOne);
		stage.sizeToScene();
		// Current snake controller for outside methods
		this.currentSnake = this.snakeOne;
		this.currentSnakeController = this.snakeControllerOne;
		// Init second snake snakeController for multiplayer if needed
		if(this.isMultiplayer) {
			this.snakeTwo = new Snake(this, this.map, this.gameUi, 1);
			this.snakeTwo.setName(username+"2");
			this.snakeControllerTwo = new SnakeController(this.snakeTwo);
			this.gameUi.getScene().addEventHandler(KeyEvent.KEY_PRESSED, this.snakeControllerTwo);
		}

		// Keyboard inputs

		this.speed = this.speedModifier * this.SPEED_BASE;
		
		this.music = new Media(getClass().getClassLoader().getResource(EnumMusic.Music01.toString()).toExternalForm());
		this.musicManager = new MediaPlayer(this.music);
		this.musicManager.setVolume(this.musicManager.getVolume() / 2);
		
	}


	/**
	 * Start the tick event
	 */
	public void play() {
		// Fill current map with transparent pictures
		// Real floor is managed by css stylesheet
		this.initFloor();
		//set the snake username
		this.snakeOne.setName(username);
		// Let's manage the current gamemode
		this.spSpawnSnake();
		if(isMultiplayer) {
			//set the snake username
			this.snakeTwo.setName(username+"2");
			this.mpSpawnSnake();
		}
		this.spSpawnSnake();
		// Let's spawn others elements
		this.initFood();
		this.initObstacles();
		if(!menu.isMuted()) {
			this.playMusic();
		}
		// Bind snake controller and keyboadr event
		// start the event tick below (-_-)
		this.animation = new AnimationTimer() {
			private long lastUpdate = 0;
			private final long NANO_MULTIPLIER = 1000000;

			@Override
			public void handle(long now) {
				if(now - lastUpdate > speed * NANO_MULTIPLIER) {
					tick();
					lastUpdate = now;
				}
			}
		};
		this.animation.start();
	}

	/**
	 * Spawn snake for singleplayer
	 */
	public void spSpawnSnake() {
		int centerX = this.map.getSize() / 2;
		int centerY = this.map.getSize() / 2;
		this.map.spawnElement(this.snakeOne.getHead(), centerY, centerX - 1);
		this.map.spawnElement(this.snakeOne.getBodyParts().get(1), centerY, centerX);
		this.map.spawnElement(this.snakeOne.getTail(), centerY, centerX + 1);
	}

	/**
	 * Spawn snakes for multiplayer
	 */
	public void mpSpawnSnake() {
		int centerX = this.map.getSize() / 2;
		int centerY = this.map.getSize() / 2;
		this.map.spawnElement(this.snakeTwo.getHead(), centerY - 2, centerX - 1);
		this.map.spawnElement(this.snakeTwo.getBodyParts().get(1), centerY - 2, centerX);
		this.map.spawnElement(this.snakeTwo.getTail(), centerY - 2, centerX + 1);
	}

	/**
	 * Start the music !
	 * We pick a random music from our EnumMusic
	 */
	public void playMusic() {
		/* Making sure we restart the music one it has been stopped
		 And we shuffle the next music */
		this.musicManager.setOnEndOfMedia(new Runnable() {
			@Override
			public void run() {
				musicManager.seek(Duration.ZERO);
			}
		});
		this.musicManager.play();
	}

	/**
	 * Event tick called by game thread
	 */
	protected void tick() {
		// Update the snake movement
		try {
			if(!isMultiplayer) {
				this.currentSnake = this.snakeOne;
				this.snakeOne.move();
			}
			else {
				this.currentSnake = this.snakeOne;
				this.currentSnake.move();
				this.currentSnake = this.snakeTwo;
				this.currentSnake.move();
			}
		} catch(Exception e) {
			System.out.println(e);
			//System.exit(1);
		}
	}

	/**
	 * Init all cells with floor element
	 */
	public void initFloor() {
		for(int y = 0; y < this.map.getSize(); y++) {
			for(int x = 0; x < this.map.getSize(); x++) {
				Floor floor = new Floor();
				this.map.getChildren().add(floor);
				this.map.getElements()[y][x] = floor;
			}
		}
	}

	/**
	 * Init the obstacles, snakeLocation + 1 around the snake
	 */
	public void initObstacles() {
		for(int i = 0; i < this.map.setObstacleAmount(Math.random() * 6 + 2); i++) {
			this.map.spawnScatter(new Obstacle());
		}
	}

	/**
	 * Init the first food element
	 */
	public void initFood() {
		this.map.spawnScatter(new Food());
	}

	/**
	 * Is the game over ?
	 */
	public void gameOver() {
		System.out.println("" +
			"   _________    __  _________   ____ _    ____________ \n" +
			"  / ____/   |  /  |/  / ____/  / __ \\ |  / / ____/ __ \\\n" +
			" / / __/ /| | / /|_/ / __/    / / / / | / / __/ / /_/ /\n" +
			"/ /_/ / ___ |/ /  / / /___   / /_/ /| |/ / /___/ _, _/ \n" +
			"\\____/_/  |_/_/  /_/_____/   \\____/ |___/_____/_/ |_|  ");
		tickPause();
		gameUi.gameOver();
	}
	
	/**
	 * Is the game win ?
	 */
	public void gameWin() {
		System.out.println("" +
			"   _________    __  _________   _       _______   __\n" +
			"  / ____/   |  /  |/  / ____/  | |     / /  _/ | / /\n" +
			" / / __/ /| | / /|_/ / __/     | | /| / // //  |/ / \n" +
			"/ /_/ / ___ |/ /  / / /___     | |/ |/ // // /|  /  \n" +
		   "\\____/_/  |_/_/  /_/_____/     |__/|__/___/_/ |_/   ");
		tickPause();
		gameUi.gameWin();
	}

	public void upScore(int i) {
		score += i;
		this.gameUi.upScore(i);
	}

	/**
	 * Let's pause our timeline and game logic
	 */
	public void tickPause() {
		this.animation.stop();
		this.musicManager.pause();
	}

	/**
	 * Let's continue to play the game
	 */
	public void tickContinue() {
		this.animation.start();
		if(!menu.isMuted()) {
			this.musicManager.play();
		}
	}


	public void newGame() {
		game = new Game(App.getStage(), true);
		game.setMenu(menu);
	}

	/**
	 * Let's change that speed
	 */
	public void increaseSpeed(double mod) {
		this.speedModifier = this.speedModifier * mod;
		this.speed = this.SPEED_BASE * this.speedModifier;
	}
	
	public void backToMenu() {
		menu.show();
	}
	
	public void exitGame() {
		menu.closeProgram();
	}

	// ========================================================================================================== //
	// GETTERS
	// ========================================================================================================== //

	/**
	 * @return our lovely snake
	 */
	public Snake getCurrentSnake() {
		return this.currentSnake;
	}

	/**
	 * Compute the current speed :: aka time between each event tick
	 */
	public double getSpeed() {
		return this.speed;
	}

	public double getSpeedModifier() {
		return this.speedModifier;
	}

	/**
	 * @return the current gameUi reference
	 */
	public GameUi getGameUi() {
		return this.gameUi;
	}


	/**
	 * @return We are lazy guys so we return the game object from static call
	 */
	public static Game getGame() {
		return game;
	}

	/**
	 * @return the current map
	 */
	public Map getMap() {
		return map;
	}

	/**
	 * @return Which menu (has a launcher) are we linked to ?
	 */
	public Menu getMenu() {
		return menu;
	}
	
	/**
	 * @return Is the game running in multiplayer mode ?
	 */
	public boolean isMultiplayer() {
		return this.isMultiplayer;
	}

	public Stage getStage() {
		return App.getStage();
	}

	public int getScore() {
		return score;
	}

	// ========================================================================================================== //
	// SETTERS
	// ========================================================================================================== //


	public void setScore(int score) {
		this.score = score;
	}

	/**
	 * Setter for the menu
	 * (Required to go back to the main menu when we start a new game.)
	 * @param originalMenu
	 */
	public void setMenu(Menu originalMenu) {
		menu = originalMenu;
	}

	public static void setGame(Game game) {
		Game.game = game;
	}

	public void setMap(Map map) {
		this.map = map;
	}

	/**
	 * @return set our lovely snake
	 */
	public void setCurrentSnake(Snake s) {
		this.currentSnake = s;
	}
}
