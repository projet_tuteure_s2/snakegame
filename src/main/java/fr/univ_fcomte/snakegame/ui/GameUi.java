package fr.univ_fcomte.snakegame.ui;

import fr.univ_fcomte.snakegame.App;
import fr.univ_fcomte.snakegame.Game;
import fr.univ_fcomte.snakegame.map.Map;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class GameUi {
	
	private Stage stage;
	private Map map;
	private Scene scene;
	private Game game;
	
	private int eatScore;
	protected Text textTitle;
	protected Text eatScoreString;
	private int score;
	protected Text scoreString;
	protected GridPane sceneLayout;
	
	protected Button buttonInGame;
	
	protected VBox layoutScore;
	protected VBox layoutScoreContainer;
	protected BorderPane borderPane;
	
	
	public GameUi(Stage stage, Map map, Game game) {
		this.stage = stage;
		this.map = map;
		this.eatScore = 0;
		this.score = 0;
		this.game = game;
		
		initializeWidgets();
		addWidgetsToWindow();
	}
	
	public void initializeWidgets() {
		textTitle = new Text("Informations");
		textTitle.setId("textTitle");
		
		eatScoreString = new Text("Foods : " + this.eatScore);
		eatScoreString.setId("text");
		scoreString = new Text("Score : " + this.score);
		scoreString.setId("text");
		
		this.map.setId("map");
		
		buttonInGame = new Button();
		buttonInGame.setId("buttonInGame");
		buttonInGame.setCursor(Cursor.HAND);
		buttonInGame.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				game.tickPause();
				Stage pause = new Stage();
				pause.initModality(Modality.APPLICATION_MODAL);
				pause.setTitle("Pause");
				pause.setResizable(false);
				pause.initStyle(StageStyle.UNDECORATED);
				pause.centerOnScreen();
				
				Text textTitle = new Text("Pause");
				textTitle.setId("textTitle");
				
				Button continueGame = new Button();
				continueGame.setId("continueGame");
				continueGame.setCursor(Cursor.HAND);
				
				Button backToMenu = new Button();
				backToMenu.setId("backToMenu2");
				backToMenu.setCursor(Cursor.HAND);
				
				Button restartGame = new Button();
				restartGame.setId("restartGame");
				restartGame.setCursor(Cursor.HAND);
				
				Button endProgram = new Button();
				endProgram.setId("endProgram");
				endProgram.setCursor(Cursor.HAND);
				
				continueGame.setOnAction(e -> {
					pause.close();
					game.tickContinue();
				});

				backToMenu.setOnAction(e -> {
					pause.close();
					game.backToMenu();
				});
				
				restartGame.setOnAction(e ->{
					pause.close();
					Game.setGame(new Game(App.getStage(), Game.getGame().isMultiplayer()));
					Game.getGame().getGameUi().show();
					Game.getGame().play();
				});
				
				endProgram.setOnAction(e -> {
					pause.close();
					game.exitGame();
				});
				
				VBox layout = new VBox();
				layout.setSpacing(5);
				layout.getChildren().add(textTitle);
				layout.getChildren().add(continueGame);
				layout.getChildren().add(backToMenu);
				layout.getChildren().add(restartGame);
				layout.getChildren().add(endProgram);
				layout.setAlignment(Pos.CENTER);
				Scene scene2 = new Scene(layout, 450, 400);
				scene2.getStylesheets().add("/css/styleMenuGameUI.css");
				pause.setScene(scene2);
				pause.showAndWait();
			}
			
		});
	}

	public void addWidgetsToWindow() {
		layoutScore = new VBox();
		layoutScoreContainer = new VBox();
		layoutScoreContainer.setAlignment(Pos.CENTER);
		layoutScoreContainer.setId("loginForm");
		
		
		layoutScoreContainer.getChildren().addAll(textTitle, scoreString, eatScoreString, buttonInGame);
		layoutScore.getChildren().addAll(layoutScoreContainer);
		BorderPane.setAlignment(layoutScoreContainer, Pos.BASELINE_LEFT);
		VBox.setMargin(scoreString, new Insets(0, 250, 0, 0));
		VBox.setMargin(eatScoreString, new Insets(0, 250, 0, 0));
		VBox.setMargin(buttonInGame, new Insets(50, 0, 10, 0));
		VBox.setMargin(layoutScoreContainer, new Insets(0, 0, 0, 15));
		
		borderPane = new BorderPane();	
		borderPane.setLeft(this.map);
		borderPane.setCenter(layoutScore);
		
		this.scene = new Scene(borderPane, 960, 580);
		this.scene.getStylesheets().add("/css/styleGame"+this.map.getFloor().name()+".css");

	}
	
	public void show() {
		this.stage.setScene(this.scene);
	}
	
	public void upEatScore() {
		this.eatScore += 1;
		this.updateEatScore();
	}
	
	public void upScore(int i) {
		this.score += i;
		this.updateScore();
	}
	
	public void updateEatScore() {
		this.layoutScoreContainer.getChildren().remove(this.eatScoreString);
		this.eatScoreString.setText("Foods: " + this.eatScore);
		this.layoutScoreContainer.getChildren().add(2, this.eatScoreString);
		VBox.setMargin(eatScoreString, new Insets(0, 250, 0, 0));
	}
	
	public void updateScore() {
		this.layoutScoreContainer.getChildren().remove(this.scoreString);
		this.scoreString.setText("Score: " + this.score);
		this.layoutScoreContainer.getChildren().add(1, this.scoreString);
		VBox.setMargin(scoreString, new Insets(0, 250, 0, 0));
	}
	
	/**
	 * We trigger a gameOver scene, and reach back the start menu
	 */
	public void gameOver() {
		Stage gameOver = new Stage();
		gameOver.initModality(Modality.APPLICATION_MODAL);
		gameOver.setTitle("GameOver");
		gameOver.setResizable(false);
		gameOver.initStyle(StageStyle.UNDECORATED);
		gameOver.centerOnScreen();
		Button backToMenu = new Button();
		backToMenu.setCursor(Cursor.HAND);
		backToMenu.setId("backToMenu");

		
		backToMenu.setOnAction(e -> {
			gameOver.close();
			game.backToMenu();
		});
		
		VBox layout = new VBox();
		layout.setSpacing(20);
		
		Text textTitle = new Text("GAME OVER");
		textTitle.setId("textTitle");
		
		layout.getChildren().add(textTitle);
		layout.getChildren().add(backToMenu);
		layout.setAlignment(Pos.CENTER);
		Scene scene2 = new Scene(layout, 450, 200);
		scene2.getStylesheets().add("/css/styleMenuGameUI.css");
		gameOver.setScene(scene2);
		gameOver.show();
		
	}
	

	public void gameWin() {
		Stage gameWin = new Stage();
		gameWin.initModality(Modality.APPLICATION_MODAL);
		gameWin.setTitle("GameWin");
		gameWin.setResizable(false);
		gameWin.initStyle(StageStyle.UNDECORATED);
		gameWin.centerOnScreen();
		Button backToMenu = new Button();
		backToMenu.setCursor(Cursor.HAND);
		backToMenu.setId("backToMenu");
		
		backToMenu.setOnAction(e -> {
			gameWin.close();
			game.backToMenu();
		});
		
		VBox layout = new VBox();
		layout.setSpacing(20);
		
		Text textTitle = new Text("CONGRATULATION!");
		textTitle.setId("textTitle");
		
		layout.getChildren().add(textTitle);
		layout.getChildren().add(backToMenu);
		layout.setAlignment(Pos.CENTER);
		Scene scene2 = new Scene(layout, 450, 200);
		scene2.getStylesheets().add("/css/styleMenuGameUI.css");
		gameWin.setScene(scene2);
		gameWin.show();
		
	}
	
	// ========================================================================================================== //
	// GETTERS
	// ========================================================================================================== //
	
	public Scene getScene() {
		return this.scene;
	}
	
	/**
	 * Return the number of fruits eaten by the snake
	 *
	 * @return
	 */
	public int getEatScore() {
		return this.eatScore;
	}
	
	// ========================================================================================================== //
	// SETTERS
	// ========================================================================================================== //
	
	/**
	 * Change the eat score
	 */
	public void setEatScore(int i) {
		this.eatScore = i;
	}
	
}
