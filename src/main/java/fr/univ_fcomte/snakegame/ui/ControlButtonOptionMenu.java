package fr.univ_fcomte.snakegame.ui;

import fr.univ_fcomte.snakegame.Game;
import javafx.event.ActionEvent;

public class ControlButtonOptionMenu extends ControlButton {
	
	public ControlButtonOptionMenu(Game game, Menu menu) {
		super(game, menu);
		menu.setButtonControlerOptionMenu(this);
		menu.setButtonControlerAppaMenu(this);
	}
	
	@Override
	public void handle(ActionEvent event) {
		// Play UI sound
		super.playSound();
		// ---
		if(event.getSource().equals(menu.btnSon)) {
			if (menu.btnSon.getId().equals("btnSonOn")) {
				menu.btnSon.setId("btnSonOff");
				this.menu.setMuted(true);
			}	
			else {
				menu.btnSon.setId("btnSonOn");
				this.menu.setMuted(false);
			}
		}
		else if(event.getSource().equals(menu.btnAppa)) {
			menu.stage.setScene(menu.appaScene);
		}
		else if(event.getSource().equals(menu.btnReturn)) {
			menu.stage.setScene(menu.startScene);
		}
		else if(event.getSource().equals(menu.btnAppa)) {
			menu.stage.setScene(menu.optionScene);
		}
		else if(event.getSource().equals(menu.btnBack)) {
			menu.stage.setScene(menu.optionScene);
		}
		
	}
	
}
