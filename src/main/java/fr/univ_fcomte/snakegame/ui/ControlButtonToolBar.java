package fr.univ_fcomte.snakegame.ui;

import fr.univ_fcomte.snakegame.Game;
import javafx.event.ActionEvent;

public class ControlButtonToolBar extends ControlButton {
	
	public ControlButtonToolBar(Game game, Menu menu) {
		super(game, menu);
		menu.setButtonControlerToolBar(this);
	}
	
	@Override
	public void handle(ActionEvent event) {
		// Play UI sound
		super.playSound();
		// ---
		if(event.getSource().equals(menu.btnClose)) {
			this.menu.stage.close();
		}
		else if(event.getSource().equals(menu.btnMax)) {
			menu.stage.setFullScreen(!menu.stage.isFullScreen());
		}
		else {
			menu.stage.setIconified(!menu.stage.isIconified());
		}
		
	}
	
}
