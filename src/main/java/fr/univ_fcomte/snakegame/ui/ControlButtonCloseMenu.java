package fr.univ_fcomte.snakegame.ui;

import fr.univ_fcomte.snakegame.Game;
import javafx.event.ActionEvent;

public class ControlButtonCloseMenu extends ControlButton {
	
	public ControlButtonCloseMenu(Game game, Menu menu) {
		super(game, menu);
		this.menu.setButtonControlerCloseProg(this);
	}
	
	@Override
	public void handle(ActionEvent event) {
		// Play UI sound
		super.playSound();
		// ---
		if(event.getSource().equals(menu.btnYes)) {
			menu.stage.close();
		}
		else if(event.getSource().equals(menu.btnNo)) {
			menu.stage.setScene(menu.startScene);
		}
	}
	
}
