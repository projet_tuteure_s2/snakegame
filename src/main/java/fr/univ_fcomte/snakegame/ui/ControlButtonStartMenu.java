package fr.univ_fcomte.snakegame.ui;

import fr.univ_fcomte.snakegame.App;
import fr.univ_fcomte.snakegame.Game;
import javafx.event.ActionEvent;
import javafx.stage.Stage;

public class ControlButtonStartMenu extends ControlButton {
	
	Stage stage;
	
	public ControlButtonStartMenu(Game game, Menu menu) {
		super(game, menu);
		menu.setButtonControlerStartMenu(this);
	}
	
	@Override
	public void handle(ActionEvent event) {
		// Play UI sound
		super.playSound();
		// ---
		if(event.getSource().equals(menu.btnOption)) {
			menu.stage.setScene(menu.optionScene);
		}
		else if(event.getSource().equals(menu.btnExit)) {
			menu.closeProgram();
		}
		else if(event.getSource().equals(menu.btnMulti)) {
			System.out.println("Game is working in multiplayer...");
			Game.setGame(new Game(App.getStage(), true));
			Game.getGame().getGameUi().show();
			Game.getGame().play();
		}
		else if(event.getSource().equals(menu.btnPlay)) {
			System.out.println("Game is working...");
			Game.setGame(new Game(App.getStage(), false));
			Game.getGame().getGameUi().show();
			Game.getGame().play();
		}
		
	}
	
	
}
