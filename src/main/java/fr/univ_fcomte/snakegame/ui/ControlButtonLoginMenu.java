package fr.univ_fcomte.snakegame.ui;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.univ_fcomte.snakegame.Game;

public class ControlButtonLoginMenu extends ControlButton {
	
	public ControlButtonLoginMenu(Game game, Menu menu) {
		super(game, menu);
		menu.setButtonControlerLogin(e -> {
			// Play UI sound
			super.playSound();
			// ---
			String s = this.menu.getInputName();
			if(!this.menu.getInputName().replace(" ", "").equals("")) {
				Pattern p = Pattern.compile("[^A-Za-z0-9]");
			    Matcher m = p.matcher(s);
			    if(m.find()) {
			    	this.menu.errorMes.setText("Please enter your username without specials characters");
			    }
			    else {
			    	this.menu.stage.setScene(this.menu.startScene);
			    	Game.username=this.menu.getInputName();
					game.getCurrentSnake().setName(this.menu.getInputName());
					this.menu.footerPseudoName.setText(this.game.getCurrentSnake().getName());
			    }
			}
			else {
				this.menu.errorMes.setText("Please enter your username");
			}
		});
	}
	
}
