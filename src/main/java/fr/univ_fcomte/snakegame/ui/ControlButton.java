package fr.univ_fcomte.snakegame.ui;

import fr.univ_fcomte.snakegame.Game;
import fr.univ_fcomte.snakegame.enums.EnumUiSound;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class ControlButton implements EventHandler<ActionEvent> {
	
	protected Game game;
	protected Menu menu;
	protected GameUi gameUi;
	
	
	public ControlButton(Game game, Menu menu) {
		this.game = game;
		this.menu = menu;
	}
	
	public ControlButton(Game game, GameUi gameUi, Menu menu) {
		this(game, menu);
		this.gameUi = gameUi;
	}
	
	@Override
	public void handle(ActionEvent event) {
		
	}
	
	/**
	 * Let's play a sound when required
	 * (Clicked, maybe hovered too ?)
	 */
	public void playSound() {
		// Only if the menu allows it
		if(!this.menu.isMuted()) {
			Media media = new Media(getClass().getClassLoader().getResource(EnumUiSound.getRandomSound()).toExternalForm());
			MediaPlayer mediaPlayer = new MediaPlayer(media);
			mediaPlayer.setVolume(mediaPlayer.getVolume() / 2);
			mediaPlayer.play();
		}
	}
	
}
