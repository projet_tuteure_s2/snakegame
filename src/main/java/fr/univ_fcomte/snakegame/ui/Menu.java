package fr.univ_fcomte.snakegame.ui;

import fr.univ_fcomte.snakegame.App;
import fr.univ_fcomte.snakegame.Game;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


public class Menu {
	
	// ========================================================================================================== //
	// ATTRIBUTS
	// ========================================================================================================== //
	
	// model & stage
	protected Game game;
	protected Stage stage;
	
	// toolBar
	protected ToolBar toolBar;
	protected ToolBar toolBar2;
	protected ToolBar toolBar3;
	protected ToolBar toolBar4;
	protected ToolBar toolBar5;
	
	// Button close
	protected Image CloseBtnImg;
	protected ImageView CloseBtnImgView;
	protected Button btnClose;
	
	// Button maximize
	protected Image maxBtnImg;
	protected ImageView maxBtnImgView;
	protected Button btnMax;
	
	// Button minimize
	protected Image minBtnImg;
	protected ImageView minBtnImgView;
	protected Button btnMin;
	
	// Region
	protected Region spacer;
	
	// ========================================================================================================== //
	//    LOGIN MENU
	// ========================================================================================================== //
	
	// logo
	protected Image logo;
	protected ImageView logoImgView;
	protected ImageView logoImgView2;
	
	//userName
	protected Text userName;
	protected TextField userTextField;
	protected Text errorMes;
	
	// Button connect
	protected Button btnConnect;
	
	// Spaces
	protected Text space;
	protected Text space2;
	
	// Box
	protected VBox layoutLogin;
	protected VBox layoutFormLogin;
	
	// footer text
	protected Text footerText;
	protected Text footerText2;
	protected Text footerPseudoName;
	protected Text footerText4;
	
	// Boder pane & HBox
	protected BorderPane borderPane;
	protected BorderPane borderPane4;
	protected HBox statusbar;
	protected Scene loginScene;
	
	// ========================================================================================================== //
	// START MENU
	// ========================================================================================================== //
	
	protected VBox layoutMenu;
	protected VBox layoutMenuContainer;
	
	// Text
	protected BorderPane borderPane2;
	protected HBox statusbar2;
	
	// Buttons
	protected Button btnOption;
	protected Button btnPlay;
	protected Button btnExit;
	protected Button btnMulti;
	
	// Layout
	protected Scene startScene;
	
	
	// Option menu: button
	protected Text textOptions;
	protected Text textSound;
	protected Button btnSon;
	protected Button btnAppa;
	protected Button btnReturn;
	protected VBox layoutOption;
	protected VBox layoutOptionContainer;
	protected Scene optionScene;
	protected HBox statusbar4;
	
	// Style menu: button
	protected Button btnBack;
	
	// Style menu: layout
	protected VBox layoutAppa;
	protected VBox layoutAppaContainer;
	protected Text textAppa;
	protected Scene appaScene;
	protected BorderPane borderPane5;
	
	// DIM
	protected double WIDTH = App.getDimensions()[0];
	protected double HEIGHT = App.getDimensions()[1];
	
	// Close program
	protected Scene exitScene;
	protected Text textTitle;
	protected Text textExit;
	protected Button btnYes;
	protected Button btnNo;
	protected VBox layoutExit;
	protected VBox layoutExitContainer;
	protected BorderPane borderPane3;
	protected HBox statusbar3;
	
	// ========================================================================================================== //
	// AUDIO
	// ========================================================================================================== //
	
	private boolean muted;
	
    private double x = 0;
    private double y = 0;
	
	/**
	 * @param game
	 * @param stage
	 */
	public Menu(Game game, Stage stage) {
		this.game = game;
		this.stage = stage;
		initializeWidgets();
		addWidgetsToWindow();
		this.stage.setResizable(false);
		this.stage.initStyle(StageStyle.UNDECORATED);
		this.stage.centerOnScreen();
		this.stage.getIcons().add(new Image("/img/logo.png"));
		this.stage.setTitle("SnakeVenom Game");
		this.stage.setOnCloseRequest(e -> {
			e.consume();
			closeProgram();
		});
	}
	
	/**
	 * Widget initialisation
	 */
	public void initializeWidgets() {
		
		// Tool bar
		toolBar = new ToolBar();
		toolBar.setId("toolBar");
		
		// Button close
		CloseBtnImgView = new ImageView(CloseBtnImg);
		CloseBtnImgView.setFitWidth(40);
		CloseBtnImgView.setFitHeight(1);
		btnClose = new Button("", CloseBtnImgView);
		btnClose.setCursor(Cursor.HAND);
		btnClose.setId("btnClose");
		
		// Button maximize
		maxBtnImgView = new ImageView(maxBtnImg);
		maxBtnImgView.setFitWidth(30);
		maxBtnImgView.setFitHeight(1);
		btnMax = new Button("", maxBtnImgView);
		btnMax.setCursor(Cursor.HAND);
		btnMax.setId("btnMax");
		
		// Button minimize
		minBtnImgView = new ImageView(minBtnImg);
		minBtnImgView.setFitWidth(30);
		minBtnImgView.setFitHeight(1);
		btnMin = new Button("", minBtnImgView);
		btnMin.setCursor(Cursor.HAND);
		btnMin.setId("btnMin");
		
		// Region
		spacer = new Region();
		
		// Launch Screen: label, button, text field
		logo = new Image("/img/SnakeVenomLogo.png");
		logoImgView = new ImageView(logo);

		logoImgView.setFitWidth(WIDTH / 3);
		logoImgView.setFitHeight(HEIGHT / 3);
		
		userName = new Text("USERNAME :");
		userName.setId("text");
		errorMes = new Text("");
		errorMes.setId("errorText");
		
		btnConnect = new Button();
		btnConnect.setId("btnConnect");
		btnConnect.setCursor(Cursor.HAND);
		
		userTextField = new TextField();
		userTextField.setId("PseudoField");
		
		layoutLogin = new VBox(toolBar);
		layoutFormLogin = new VBox();
		layoutFormLogin.setAlignment(Pos.CENTER);
		layoutFormLogin.setId("loginForm");
		
		footerText = new Text("Evan DUBOIS - Romain GONCALVES - Hugo GOYARD - Serkan KAYA - Axel MIOTTE - Hugo ROYEN");
		footerText.setId("footerTxt");
		
		borderPane = new BorderPane();
		borderPane.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				x = event.getSceneX();
				y = event.getSceneY();
			}
		});
		borderPane.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				stage.setX(event.getScreenX() - x);
				stage.setY(event.getScreenY() - y);
			}
		});
		statusbar = new HBox();
		statusbar.setId("footer");
		statusbar.setAlignment(Pos.CENTER);
		
		
		// Main menu: label, button
		
		// Tool bar
		toolBar2 = new ToolBar();
		toolBar2.setId("toolBar");
		
		// Button close
		CloseBtnImgView = new ImageView(CloseBtnImg);
		CloseBtnImgView.setFitWidth(40);
		CloseBtnImgView.setFitHeight(1);
		btnClose = new Button("", CloseBtnImgView);
		btnClose.setCursor(Cursor.HAND);
		btnClose.setId("btnClose");
		
		// Button maximize
		maxBtnImgView = new ImageView(maxBtnImg);
		maxBtnImgView.setFitWidth(30);
		maxBtnImgView.setFitHeight(1);
		btnMax = new Button("", maxBtnImgView);
		btnMax.setCursor(Cursor.HAND);
		btnMax.setId("btnMax");
		
		// Button minimize
		minBtnImgView = new ImageView(minBtnImg);
		minBtnImgView.setFitWidth(30);
		minBtnImgView.setFitHeight(1);
		btnMin = new Button("", minBtnImgView);
		btnMin.setCursor(Cursor.HAND);
		btnMin.setId("btnMin");
		
		// Region
		spacer = new Region();
		
		logoImgView2 = new ImageView(logo);
		logoImgView2.setFitWidth(WIDTH / 3);
		logoImgView2.setFitHeight(HEIGHT / 3);
		
		btnPlay = new Button();
		btnPlay.setId("btnPlay");
		btnPlay.setCursor(Cursor.HAND);
		
		btnMulti = new Button();
		btnMulti.setId("btnMulty");
		btnMulti.setCursor(Cursor.HAND);
		
		btnOption = new Button();
		btnOption.setId("btnOption");
		btnOption.setCursor(Cursor.HAND);
		
		btnExit = new Button();
		btnExit.setId("btnExit");
		btnExit.setCursor(Cursor.HAND);
		
		layoutMenu = new VBox(toolBar2);
		layoutMenuContainer = new VBox();
		layoutMenuContainer.setAlignment(Pos.CENTER);
		layoutMenuContainer.setId("loginForm");
		
		footerText2 = new Text("Signed in as :");
		footerText2.setId("footerTxt");
		footerPseudoName = new Text("");
		footerPseudoName.setId("footerPseudo");
		
		borderPane2 = new BorderPane();
		borderPane2.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				x = event.getSceneX();
				y = event.getSceneY();
			}
		});
		borderPane2.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				stage.setX(event.getScreenX() - x);
				stage.setY(event.getScreenY() - y);
			}
		});

		statusbar2 = new HBox();
		statusbar2.setAlignment(Pos.BOTTOM_LEFT);
		
		
		// Option menu: button
		
		toolBar4 = new ToolBar();
		toolBar4.setId("toolBar");
		
		// Button close
		CloseBtnImgView = new ImageView(CloseBtnImg);
		CloseBtnImgView.setFitWidth(40);
		CloseBtnImgView.setFitHeight(1);
		btnClose = new Button("", CloseBtnImgView);
		btnClose.setCursor(Cursor.HAND);
		btnClose.setId("btnClose");
		
		// Button maximize
		maxBtnImgView = new ImageView(maxBtnImg);
		maxBtnImgView.setFitWidth(30);
		maxBtnImgView.setFitHeight(1);
		btnMax = new Button("", maxBtnImgView);
		btnMax.setCursor(Cursor.HAND);
		btnMax.setId("btnMax");
		
		// Button minimize
		minBtnImgView = new ImageView(minBtnImg);
		minBtnImgView.setFitWidth(30);
		minBtnImgView.setFitHeight(1);
		btnMin = new Button("", minBtnImgView);
		btnMin.setCursor(Cursor.HAND);
		btnMin.setId("btnMin");
		
		
		textOptions = new Text("Options");
		textOptions.setId("textOptions");
		
		textSound = new Text("SOUND");
		textSound.setId("textSound");
		
		btnSon = new Button();
		btnSon.setId("btnSonOn");
		btnSon.setCursor(Cursor.HAND);
		
		btnAppa = new Button();
		btnAppa.setId("btnAppa");
		btnAppa.setCursor(Cursor.HAND);
		
		btnReturn = new Button();
		btnReturn.setId("btnReturn");
		btnReturn.setCursor(Cursor.HAND);
		
		layoutOption = new VBox(toolBar4);
		layoutOptionContainer = new VBox();
		layoutOptionContainer.setAlignment(Pos.CENTER);
		layoutOptionContainer.setId("layoutOptionContainer");
		
		footerText4 = new Text(" ");
		
		borderPane4 = new BorderPane();
		borderPane4.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				x = event.getSceneX();
				y = event.getSceneY();
			}
		});
		borderPane4.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				stage.setX(event.getScreenX() - x);
				stage.setY(event.getScreenY() - y);
			}
		});
		statusbar4 = new HBox();
		statusbar4.setAlignment(Pos.CENTER);
		
		
		// Apparence menu:
		toolBar5 = new ToolBar();
		toolBar5.setId("toolBar");
		
		// Button close
		CloseBtnImgView = new ImageView(CloseBtnImg);
		CloseBtnImgView.setFitWidth(40);
		CloseBtnImgView.setFitHeight(1);
		btnClose = new Button("", CloseBtnImgView);
		btnClose.setCursor(Cursor.HAND);
		btnClose.setId("btnClose");
		
		// Button maximize
		maxBtnImgView = new ImageView(maxBtnImg);
		maxBtnImgView.setFitWidth(30);
		maxBtnImgView.setFitHeight(1);
		btnMax = new Button("", maxBtnImgView);
		btnMax.setCursor(Cursor.HAND);
		btnMax.setId("btnMax");
		
		// Button minimize
		minBtnImgView = new ImageView(minBtnImg);
		minBtnImgView.setFitWidth(30);
		minBtnImgView.setFitHeight(1);
		btnMin = new Button("", minBtnImgView);
		btnMin.setCursor(Cursor.HAND);
		btnMin.setId("btnMin");
		
		textAppa = new Text("Section still in development...");
		textAppa.setId("textAppa");
		btnBack = new Button();
		btnBack.setCursor(Cursor.HAND);
		btnBack.setId("btnBack");
		
		layoutAppa = new VBox(toolBar5);
		layoutAppaContainer = new VBox();
		layoutAppaContainer.setAlignment(Pos.CENTER);
		layoutAppaContainer.setId("layoutAppaContainer");
		
		borderPane5 = new BorderPane();
		borderPane5.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				x = event.getSceneX();
				y = event.getSceneY();
			}
		});
		borderPane5.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				stage.setX(event.getScreenX() - x);
				stage.setY(event.getScreenY() - y);
			}
		});
		
		
		// Close program
		toolBar3 = new ToolBar();
		toolBar3.setId("toolBar");
		
		textTitle = new Text("Quit");
		textTitle.setId("textTitle");
		
		textExit = new Text("Are you sure you want to exit SnakeVenom?");
		textExit.setId("textExit");
		
		btnYes = new Button();
		btnYes.setId("btnYes");
		btnYes.setCursor(Cursor.HAND);
		
		btnNo = new Button();
		btnNo.setId("btnNo");
		btnNo.setCursor(Cursor.HAND);
				
		
		layoutExit = new VBox(toolBar3);
		layoutExitContainer = new VBox();
		layoutExitContainer.setAlignment(Pos.TOP_LEFT);
	
		
		borderPane3 = new BorderPane();
		borderPane3.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				x = event.getSceneX();
				y = event.getSceneY();
			}
		});
		borderPane3.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				stage.setX(event.getScreenX() - x);
				stage.setY(event.getScreenY() - y);
			}
		});
		statusbar3 = new HBox();
		statusbar3.setAlignment(Pos.BOTTOM_RIGHT);
		
		this.muted = false;
	}
	
	/**
	 * Add widgets to windows after initalization
	 */
	private void addWidgetsToWindow() {
		HBox.setHgrow(spacer, Priority.ALWAYS);
		toolBar.getItems().addAll(spacer, btnMin, btnMax, btnClose);
		
		layoutFormLogin.getChildren().addAll(logoImgView, userName, userTextField, errorMes, btnConnect);
		VBox.setMargin(userName, new Insets(0, 320, 0, 0));
		VBox.setMargin(btnConnect, new Insets(20, 0, 20, 0));
		layoutLogin.getChildren().addAll(layoutFormLogin);
		BorderPane.setAlignment(layoutFormLogin, Pos.CENTER);
		
		statusbar.getChildren().addAll(footerText);
		HBox.setMargin(footerText, new Insets(20, 0, 20, 0));
		borderPane.setTop(layoutLogin);
		borderPane.setCenter(layoutFormLogin);
		borderPane.setBottom(statusbar);
		
		loginScene = new Scene(borderPane, WIDTH, HEIGHT);
		loginScene.getStylesheets().add("/css/styleLogin.css");
		
		HBox.setHgrow(spacer, Priority.ALWAYS);
		toolBar2.getItems().addAll(spacer, btnMin, btnMax, btnClose);
		

		layoutMenuContainer.getChildren().addAll(logoImgView2, btnPlay, btnMulti, btnOption, btnExit);
		VBox.setMargin(btnPlay, new Insets(0, 0, 10, 0));
		VBox.setMargin(btnMulti, new Insets(0, 0, 10, 0));
		VBox.setMargin(btnOption, new Insets(0, 0, 10, 0));
		layoutMenu.getChildren().addAll(layoutMenuContainer);
		BorderPane.setAlignment(layoutMenuContainer, Pos.CENTER);
		
		statusbar2.getChildren().addAll(footerText2, footerPseudoName);
		HBox.setMargin(footerText2, new Insets(0, 0, 5, 5));
		HBox.setMargin(footerPseudoName, new Insets(0, 0, 5, 5));
		borderPane2.setTop(layoutMenu);
		borderPane2.setCenter(layoutMenuContainer);
		borderPane2.setBottom(statusbar2);
		
		
		startScene = new Scene(borderPane2, WIDTH, HEIGHT);
		startScene.getStylesheets().add("/css/styleStart.css");
		
		HBox.setHgrow(spacer, Priority.ALWAYS);
		toolBar4.getItems().addAll(spacer, btnMin, btnMax, btnClose);
		
		layoutOptionContainer.getChildren().addAll(textOptions,textSound, btnSon, btnAppa, btnReturn);
		VBox.setMargin(textOptions, new Insets(0, 0, 40, 0));
		VBox.setMargin(textSound, new Insets(0, 350, 0, 0));
		VBox.setMargin(btnAppa, new Insets(10, 0, 90, 0));
		layoutOption.getChildren().addAll(layoutOptionContainer);
		BorderPane.setAlignment(layoutOptionContainer, Pos.CENTER);
		
		statusbar4.getChildren().addAll(footerText4);
		borderPane4.setTop(layoutOption);
		borderPane4.setCenter(layoutOptionContainer);
		borderPane4.setBottom(statusbar4);
		
		optionScene = new Scene(borderPane4, WIDTH, HEIGHT);
		optionScene.getStylesheets().add("/css/styleOption.css");
		
		HBox.setHgrow(spacer, Priority.ALWAYS);
		toolBar5.getItems().addAll(spacer, btnMin, btnMax, btnClose);
		
		
		layoutAppaContainer.getChildren().addAll(textAppa, btnBack);
		layoutAppa.getChildren().addAll(layoutAppaContainer);
		BorderPane.setAlignment(layoutAppaContainer, Pos.CENTER);
		
		
		borderPane5.setTop(layoutAppa);
		borderPane5.setCenter(layoutAppaContainer);
		
		appaScene = new Scene(borderPane5, WIDTH, HEIGHT);
		appaScene.getStylesheets().add("/css/styleSkins.css");
		
		toolBar3.getItems().addAll(textTitle);
		
		layoutExitContainer.getChildren().addAll(textExit);
		layoutExit.getChildren().addAll(layoutExitContainer);
		VBox.setMargin(layoutExitContainer, new Insets(0, 0, 0,10));
		BorderPane.setAlignment(layoutExitContainer, Pos.TOP_LEFT);
		
		statusbar3.getChildren().addAll(btnYes, btnNo);
		HBox.setMargin(btnYes, new Insets(0, 5, 5, 0));
		HBox.setMargin(btnNo, new Insets(0, 5, 5, 0));
		borderPane3.setTop(layoutExit);
		borderPane3.setBottom(statusbar3);
		
		exitScene = new Scene(borderPane3, 569, 342);
		exitScene.getStylesheets().add("/css/styleExit.css");
		
		stage.setScene(loginScene);
	}
	
	public void display() {
		stage.show();
	}
	
	public void show() {
		this.stage.setScene(this.startScene);
	}
	
	/**
	 * Close program with confirmation window
	 */
	public void closeProgram() {
		stage.setScene(exitScene);
		stage.centerOnScreen();
	}
	
	
	// ========================================================================================================== //
	// CONTROLLERS
	// ========================================================================================================== //
	
	/**
	 * Controller for login button
	 *
	 * @param handler
	 */
	public void setButtonControlerLogin(EventHandler<ActionEvent> handler) {
		btnConnect.setOnAction(handler);
	}
	
	/**
	 * Controller for top toolbar
	 *
	 * @param handler
	 */
	public void setButtonControlerToolBar(EventHandler<ActionEvent> handler) {
		btnClose.setOnAction(handler);
		btnMax.setOnAction(handler);
		btnMin.setOnAction(handler);
	}
	
	/**
	 * Controller for start menu
	 *
	 * @param handler
	 */
	public void setButtonControlerStartMenu(EventHandler<ActionEvent> handler) {
		btnOption.setOnAction(handler);
		btnExit.setOnAction(handler);
		btnPlay.setOnAction(handler);
		btnMulti.setOnAction(handler);
	}
	
	/**
	 * Controller for option menu
	 *
	 * @param handler
	 */
	public void setButtonControlerOptionMenu(EventHandler<ActionEvent> handler) {
		btnSon.setOnAction(handler);
		btnAppa.setOnAction(handler);
		btnReturn.setOnAction(handler);
	}
	
	/**
	 * Controller for the apparence menu
	 *
	 * @param handler
	 */
	public void setButtonControlerAppaMenu(EventHandler<ActionEvent> handler) {
		btnBack.setOnAction(handler);
	}
	
	/**
	 * Controller for the exit confirmation
	 *
	 * @param handler
	 */
	public void setButtonControlerCloseProg(EventHandler<ActionEvent> handler) {
		btnYes.setOnAction(handler);
		btnNo.setOnAction(handler);
	}
	
	/**
	 * Enable or not the sound
	 */
	public void setMuted(boolean state) {
		this.muted = state;
	}
	
	// ========================================================================================================== //
	// GETTERS
	// ========================================================================================================== //
	
	public String getInputName() {
		return this.userTextField.getText();
	}
	
	public boolean isMuted() {
		return this.muted;
	}
}
