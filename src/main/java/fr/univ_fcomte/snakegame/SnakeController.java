package fr.univ_fcomte.snakegame;

import fr.univ_fcomte.snakegame.elements.snake.Snake;
import fr.univ_fcomte.snakegame.enums.EnumFacing;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.awt.im.InputContext;

public class SnakeController implements EventHandler<KeyEvent> {
	
	private final Snake snake;
	private final KeyCode[] layoutEN = {
		KeyCode.W,
		KeyCode.A,
		KeyCode.S,
		KeyCode.D,
	};
	private final KeyCode[] layoutFR = {
		KeyCode.Z,
		KeyCode.Q,
		KeyCode.S,
		KeyCode.D,
	};
	private final KeyCode[] layoutARROWS = {
		KeyCode.UP,
		KeyCode.LEFT,
		KeyCode.DOWN,
		KeyCode.RIGHT
	};
	private KeyCode[] layout;
	
	/**
	 * Snake controller constructor for snake object
	 * @param snake
	 */
	public SnakeController(Snake snake) {
		this.snake = snake;
		if(this.snake.getSnakeId() == 0) {
			this.player1KeyboardLayout();
		}
		if(this.snake.getSnakeId() == 1) {
			this.player2KeyboardLayout();
		}
	}
	
	/**
	 * Singleplayer keyboard layout
	 */
	private void player1KeyboardLayout() {
		InputContext context = InputContext.getInstance();
		System.out.println(context.getLocale().toString());
		switch(context.getLocale().toString()) {
			case "fr_FR":
				this.layout = this.layoutFR;
				break;
			case "en_US":
			default:
				this.layout = this.layoutEN;
				break;
		}
	}
	
	/**
	 * Multiplyaer keyboard layout
	 */
	private void player2KeyboardLayout() {
		this.layout = this.layoutARROWS;
	}
	
	/**
	 * Handling player's input from keyboard
	 * @param event
	 */
	@Override
	public void handle(KeyEvent event) {
		if(event.getCode() == this.layout[0]) {
			this.snake.turn(EnumFacing.N);
		}
		if(event.getCode() == this.layout[1]) {
			this.snake.turn(EnumFacing.W);
		}
		if(event.getCode() == this.layout[2]) {
			this.snake.turn(EnumFacing.S);
		}
		if(event.getCode() == this.layout[3]) {
			this.snake.turn(EnumFacing.E);
		}
		event.consume();
	}
}
