package fr.univ_fcomte.snakegame.map;

import fr.univ_fcomte.snakegame.Game;
import fr.univ_fcomte.snakegame.elements.Element;
import fr.univ_fcomte.snakegame.elements.Floor;
import fr.univ_fcomte.snakegame.elements.Food;
import fr.univ_fcomte.snakegame.elements.Obstacle;
import fr.univ_fcomte.snakegame.elements.snake.parts.BodyCorner;
import fr.univ_fcomte.snakegame.elements.snake.parts.BodyPart;
import fr.univ_fcomte.snakegame.elements.snake.parts.Head;
import fr.univ_fcomte.snakegame.elements.snake.parts.Tail;
import fr.univ_fcomte.snakegame.enums.EnumFloor;
import javafx.scene.layout.TilePane;

public class Map extends TilePane {
	
	private Element[][] elements;
	private int mapSize;
	public Food currentFood;
	private double obstacleAmount;
	private EnumFloor floor;
	
	/**
	 * Let's construct our map with a given size
	 * @param mapSize
	 */
	public Map(int mapSize) {
		this.setPrefColumns(mapSize);
		this.mapSize = mapSize;
		this.elements = new Element[this.mapSize][this.mapSize];
		this.floor=EnumFloor.grass1;
	}
	
	public Map(int mapSize, EnumFloor floor) {
		this.setPrefColumns(mapSize);
		this.mapSize = mapSize;
		this.elements = new Element[this.mapSize][this.mapSize];
		this.floor=floor;
	}
	
	public void show() {
	}
	
	/**
	 * Update specific cell location of the map
	 * @param x
	 * @param y
	 */
	public void updateCell(int y, int x) {
		// Computing user friendly variables
		Element element = this.elements[y][x];
		int index = coordinatesToIndex(y, x);
		// Checking all scenarios
		if(element instanceof Floor) {
			this.getChildren().set(index, new Floor());
		}
		else if(element instanceof Food) {
			this.getChildren().set(index, new Food((Food)element));
		}
		else if(element instanceof Head) {
			this.getChildren().set(index, new Head((Head)element));
		}
		else if(element instanceof Tail) {
			this.getChildren().set(index, new Tail((Tail)element));
		}
		else if(element instanceof BodyCorner) {
			BodyPart ll = Game.getGame().getCurrentSnake().getLl();
			if(ll.isCorner()) {
/*				int r = 0;
				EnumCorner lllc = ((BodyCorner) ll).getCorner();
				EnumFacing d = element.getDirection();
				if(lllc.getNext().equals(EnumFacing.E)) {
					if(lllc.getPrev().equals(EnumFacing.S)){
						if(d.equals(EnumFacing.N)) {
							r+=-90;
						}else {
							r+=+180;
						}
					}else {
						if(d.equals(EnumFacing.N)) {
							r+=-180;
						}else {
							r+=90;
						}
					}
				}else if(lllc.getNext().equals(EnumFacing.S)) {
					if(lllc.getPrev().equals(EnumFacing.W)){
						if(d.equals(EnumFacing.E)) {
							r+=-90;
						}else {
							r+=+180;
						}
					}else {
						if(d.equals(EnumFacing.E)) {
							r+=-90;
						}else {
							r+=180;
						}
					}
				}else if(lllc.getNext().equals(EnumFacing.W)) {
					if(lllc.getPrev().equals(EnumFacing.N)){
						if(d.equals(EnumFacing.S)) {
							r+=-90;
						}else {
							r+=+180;
						}
					}else {
						if(d.equals(EnumFacing.S)) {
							r+=-180;
						}else {
							r+=90;
						}
					}
				}else if(lllc.getNext().equals(EnumFacing.N)) {
					if(lllc.getPrev().equals(EnumFacing.E)){
						if(d.equals(EnumFacing.W)) {
							r+=-90;
						}else {
							r+=+180;
						}
					}else {
						if(d.equals(EnumFacing.W)) {
							r+=-180;
						}else {
							r+=90;
						}
					}
				}
				this.getChildren().set(index, new BodyCorner((BodyCorner)element, (int)((BodyCorner)ll).getRotate()+r));
			*/
				this.getChildren().set(index, new BodyCorner((BodyCorner)element, (int)element.getRotate()));
			}else {
				this.getChildren().set(index, new BodyCorner((BodyCorner)element, (int)element.getRotate()));
			}
		}
		else if(element instanceof BodyPart) {
			this.getChildren().set(index, new BodyPart((BodyPart)element));
		}
		else if(element instanceof Obstacle) {
			this.getChildren().set(index, new Obstacle());
		}
	}
	
	/**
	 * Spawn an element on the map at specific location
	 * @param element
	 * @param y
	 * @param x
	 */
	public void spawnElement(Element element, int y, int x) {
		this.elements[y][x] = element;
		this.elements[y][x].setLocationX(x);
		this.elements[y][x].setLocationY(y);
		this.updateCell(y, x);
	}
	
	public void spawnElement(Element element, int index) {
		this.spawnElement(element, this.indexToCoordinates(index)[0], this.indexToCoordinates(index)[1]);
	}
	
	/**
	 * Spawn an element randomly on the map
	 */
	public void spawnScatter(Element element) {
		int index;
		int x;
		int y;
		boolean stop = false;
		do {
			index = (int)(Math.random() * mapSize * mapSize);
			y = indexToCoordinates(index)[0];
			x = indexToCoordinates(index)[1];
			try {
				stop = ((this.elements[(y - 1) % this.mapSize][x] instanceof Floor) &&
					(this.elements[(y + 1) % this.mapSize][x] instanceof Floor) &&
					(this.elements[y][(x - 1) % this.mapSize] instanceof Floor) &&
					(this.elements[y][(x + 1) % this.mapSize] instanceof Floor)) &&
					this.getChildren().get(index) instanceof Floor;
			} catch(Exception ignore){}
		} while(!stop);
		this.spawnElement(element, index);
		if(element instanceof Food) {
			this.currentFood = (Food)element;
		}
	}
	
	/**
	 * Get the index of the TilePane from a cooridnates subset
	 * @param x
	 * @param y
	 * @return
	 */
	public int coordinatesToIndex(int y, int x) {
		return y * this.mapSize + x;
	}
	
	/**
	 * Get the coordinates of an element of the map
	 * from a TilePane index
	 * @param index
	 * @return
	 */
	public int[] indexToCoordinates(int index) {
		int[] res = {0, 0};
		res[0] = index / this.mapSize;
		res[1] = index % this.mapSize;
		return res;
	}
	
	// ========================================================================================================== //
	// GETTERS
	// ========================================================================================================== //
	
	/**
	 * Return the current set of elements on the map
	 * @return
	 */
	public Element[][] getElements() {
		return this.elements;
	}
	
	/**
	 * @return if the player has won the game
	 */
	public boolean isWin(){
		return Game.getGame().getCurrentSnake().getLength()+this.obstacleAmount>=this.mapSize ? true : false;
	}
	
	
	
	/**
	 * @return the subset containing the map size
	 */
	public int getSize() {
		return this.mapSize;
	}
	
	/**
	 * @return the current food
	 */
	public Food getCurrentFood() {
		return this.currentFood;
	}

	public EnumFloor getFloor() {
		return floor;
	}	
	
	// ========================================================================================================== //
	// SETTERS
	// ========================================================================================================== //

	public double setObstacleAmount(double d) {
		this.obstacleAmount=Math.ceil(d);
		return this.obstacleAmount;
	}

	public void setFloor(EnumFloor floor) {
		this.floor=floor;
	}
	
}
