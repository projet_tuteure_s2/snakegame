---
theme: gaia
paginate: true
_paginate: false

color: #dedede

footer: "DUBOIS Evan - GONCALVES Romain - GOYARD Hugo - KAYA Serkan - MIOTTE Axel - ROYEN Hugo"
backgroundColor: #111
backgroundImage: url("https://i.stack.imgur.com/HCXet.png")
backgroundRepeat: repeat
backgroundSize: 10em
---

![bg](https://gitlab.com/projet_tuteure_s2/snakegame/-/raw/master/src/main/resources/img/SnakeVenomLogo.png)

---

## Conception

* Utilisation **d'énumerations** :
	- EnumFood
	- EnumFacing
	- EnumMusic
* **Construction par copie** pour les éléments de la carte
* **Notion d'héritage** sur les éléments de la carte
* Possibilités de jouer avec un clavier **AZERTY ou QWERTY**

---

## Graphisme

![w:4em](../src/main/resources/img/elements/snake_head.png) ![w:4em](../src/main/resources/img/elements/snake_bodyPart.png) ![w:4em](../src/main/resources/img/elements/snake_bodyCorner.png) ![w:4em](../src/main/resources/img/elements/snake_tail.png) ![w:4em](../src/main/resources/img/elements/food_apple.png) ![w:4em](../src/main/resources/img/elements/food_mouse.png) ![w:4em](../src/main/resources/img/elements/obstacle_hildenpixel.png)

- Dimensions des assets : *64px\*64px*

---

## Règles du jeu

* Manger le plus de nourriture possible
* **Éviter** les obstacles et son corps
* Score en fonction de la nourriture mangée

### Multijoueur :

* Coopération ...
* ou rivalité !

---

## En résumé

SnakeVenom, c'est :

* Un projet JavaFX présent sur [gitlab](https://gitlab.com/projet_tuteure_s2/snakegame).
* Plus de **300 commits** en moins d'une semaine.
* Un mode **multijoueur**.
