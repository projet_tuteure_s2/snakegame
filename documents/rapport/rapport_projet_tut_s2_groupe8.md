---
titlepage: true
titlepage-color: 3c9f53
titlepage-text-color: FFFFFF
titlepage-rule-color: FFFFFF
logo: logo.png
listings-disable-line-numbers: true
disable-header-and-footer: true
title: Projet Tuteuré S2 - Snake
subtitle: Romain GONCALVES
date: 9 mars 2020
author: 
  - Evan DUBOIS
  - Hugo GOYARD
  - Serkan KAYA
  - Axel MIOTTE
  - Hugo ROYEN
header-includes: \usepackage{tikz} \usetikzlibrary{positioning,fadings,through}
---

# Présentation du jeu

Nous avons décidé de développer le classique jeu snake.
Cependant nous voulons ajouter plus de personnalisation tel que : 

- la couleur du serpent; 
- le choix du terrain; 
- un choix de pseudonyme;
- un multijoueur local
- etc...

De ce qui est du terrain nous aimerions créer un mode classique, qui reprend les règles de base de snake, un mode avec des mur au milieu du terrain et enfin un terrain multijoueur. 
Pour terminer nous voudrions ajouter un système de scoreboard proposant le classement des joueurs sur le jeu.

# Objectifs de réalisation

L'objectif principal est d'obtenir un jeu fonctionnel.
Nous allons donc nous concentrer sur ce point en priorité, puis nous y ajouteront des fonctionnalités supplémentaires par la suite.
Pour le terrain avec obstacles, lorsque le serpent touche un mur ou un obstacle, il meurt et doit recommencer une partie.
La partie multijoueur sera surement gérée en local, sur le même ordinateur.
Ainsi, lorsqu'un serpent percute un autre serpent, le premier serpent perd la partie.
Pour la partie graphique, nous souhaiterions avoir un aspect assez simpliste et old-school, en nous basant principalement sur le pixel art.


![Gradient](gradient.png)

D'autres sources d'inspiration peuvent être cités, tels que les jeux vidéo DooM 1, et DooM 2.
Il en est de même pour la partie sonore.

# Démarche et organisation

## Répartition des tâches

- Romain et Axel $\rightarrow$ programmation backend du jeu
- Hugo G $\rightarrow$ aides pour la partie graphique et pour le menu
- Evan $\rightarrow$ partie menu de navigation, interfaces homme/jeu
- Serkan $\rightarrow$ partie graphique
- Hugo R $\rightarrow$ partie son

## Outils

Pour les outils mis en place, nous utilisons principalement Gitlab, ainsi que Discord :

- Gitlab $\leftrightarrow$ outil de gestion de version, usages de différents branches, ...
- Discord $\leftrightarrow$ outil de communication, réunion, et planning
- Idea/Eclipse $\leftrightarrow$ IDE

L'usage de Discord est effectué de manière vocale et écrite, avec différents salons par thématiques.
De plus, la mise en place d'un webhook nous permet d'être notifié lors de nouveau commit sur Gitlab.

![Graphe d'avancement actuel de notre projet sur Gitlab](graph.png)

Nous sommes en permanence disponibles sur cette plateforme pour réaliser des réunions régulièrement sans rythme précis (en moyenne 2 fois par semaine).
La première nous permet de discuter de note avancement, donner quelques indications, etc ... 
La seconde permet de définir qui doit faire quoi pour la prochaine fois en fonction de ce qu'il a déjà fait.

Voici le lien de notre dépot Gitlab : **<https://gitlab.com/projet_tuteure_s2/snakegame>**.

